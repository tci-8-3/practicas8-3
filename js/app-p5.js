// MOSTRAMOS LOS RESULTADOS
const mostrarTodos = (data) => {
    const res = document.getElementById('respuesta');

    for (let item of data) {
        
        //res.innerHTML += item.userId + " " + item.id + " " + item.title + " " + item.conmplleted + "<br>"
        res.innerHTML += '<div class="container text-center"> <div class="row align-items-start">' +
            '<div class="col">'+ item.userId + '</div> <div class="col">' + item.id + '</div> <div class="col"> '
                + item.title + '</div> <div class="col">' +item.completed+ '</div> </div> </div>'
    }
}

// USANDO FETCH
const llamandoFetch = () => {
    const url = "https://jsonplaceholder.typicode.com/todos";
    fetch(url)
        .then(respuesta => respuesta.json())  
        .then(data => mostrarTodos(data))
        .catch(reject => { 
            const lblError = document.getElementById('lblError');
            lblError.innerHTML = "Surgió un error" +reject;
        });
};

// USANDO AWAIT
const llamandoAwait = async () =>{
    try{
        const url = "https://jsonplaceholder.typicode.com/todos";
        const respuesta = await fetch(url);
        const data = await respuesta.json();
        mostrarTodos(data);
    }
    catch(error){
        console.log("Surgio un error "+ error);
    }
}

// USANDO AXION

const llamandoAxion = async() =>{
    const url = "https://jsonplaceholder.typicode.com/todos";

    axios
    .get(url)
    .then((res)=>{
        mostrarTodos(res.data)
    })
    .catch((err)=>{
        console.log("Surgio un error "+err);
    })
}

const limpiarContenido = () => {
    const res = document.getElementById('respuesta');
    res.innerHTML = '';
    lblError.innerHTML = '';
};

// CODIFICAMOS LOS BOTONES
document.getElementById('btnCargarP').addEventListener('click',function(){
    llamandoFetch();
});

document.getElementById('btnCargarA').addEventListener('click',function(){
    llamandoAwait();
});

document.getElementById('btnCargarAx').addEventListener('click',function(){
    llamandoAxion();
});

btnLimpiar.addEventListener('click', limpiarContenido);

const llamandoAxion = async () => {
    const url = "https://jsonplaceholder.typicode.com/users";
    const userId = document.getElementById('id').value;

    try {
        const response = await axios.get(`${url}/${userId}`);
        const usuario = response.data;

        document.getElementById('nombre').value = usuario.name;
        document.getElementById('nombreUs').value = usuario.username;
        document.getElementById('email').value = usuario.email;
        document.getElementById('domCalle').value = usuario.address.street;
        document.getElementById('domNum').value = usuario.address.zipcode;
        document.getElementById('domCiudad').value = usuario.address.city;
    } catch (error) {
        console.log("Ocurrió un error: " + error);
    }
}


const limpiarContenido = () => {
    const id = document.getElementById('id').value = " ";
    const nombre = document.getElementById('nombre').value = " ";
    const nombreUs = document.getElementById('nombreUs').value = " ";
    const email = document.getElementById('email').value = " ";
    const domCalle = document.getElementById('domCalle').value = " ";
    const domNumero = document.getElementById('domNum').value = " ";
    const domCiudad = document.getElementById('domCiudad').value = " ";

};

// CODIFICAMOS LOS BOTONES
document.getElementById('btnBuscar').addEventListener('click',function(){
    llamandoAxion();
});
btnLimpiar.addEventListener('click', limpiarContenido);

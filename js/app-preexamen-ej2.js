// USANDO FETCH
const llamandoFetch = () => {
    const url = "https://restcountries.com/v3.1/all";
    const nomPais = document.getElementById('nomPais').value;
    const info = document.getElementById('info');

    fetch(url)
        .then(respuesta => respuesta.json())  
        .then(data => {
            console.log("Datos obtenidos:", data);
            mostrarResultados(data, nomPais);
        })
        .catch(error => { 
            console.error("Error en la solicitud fetch:", error);
            info.innerHTML = "Surgió un error: " + error;
        });
};

// BUSCAR CAPITAL Y LENGUAJE DEL PAÍS
const buscarInfoPorPais = (data, nomPais) => {
    for (let item of data) {
        const nombrePaises = Object.values(item.name);
        if (nombrePaises.includes(nomPais)) {
            return {
                capital: item.capital,
                lenguajes: Object.keys(item.languages),
                population: item.population,
                bandera: item.flags.png,
                moneda: Object.keys(item.currencies),
            };
        }
    }
    return null;  // Devuelve null si el país no se encuentra en la lista
};

// MOSTRAR RESULTADOS
const mostrarResultados = (data, nomPais) => {
    const info = document.getElementById('info');
    const resultado = buscarInfoPorPais(data, nomPais);
    console.log("Mostrar resultados:", data, nomPais);

    if (resultado) {
        info.innerHTML = `
            <p>Capital: ${resultado.capital}</p>
            <p>Población: ${resultado.population}</p>
            <p>Lenguajes: ${resultado.lenguajes.join(', ')}</p>
            <p>Moneda: ${resultado.moneda}</p>
            <img src="${resultado.bandera}" alt="Bandera.png">
            

        `;
    } else {
        info.innerHTML = "No se encontraron datos para el país ingresado.";
    }
};



const limpiarContenido = () => {
    const info = document.getElementById('info');
    const nomPais = document.getElementById('nomPais').value = " ";
    info.innerHTML = '';
};


// CODIFICAMOS LOS BOTONES
document.getElementById('btnBuscar').addEventListener('click',function(){
    llamandoFetch();
});

document.getElementById('btnLimpiar').addEventListener('click',function(){
    limpiarContenido();
});
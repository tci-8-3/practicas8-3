const mostrarTodos = (razas) => {
    const select = document.getElementById("razasCaninas");

    // Limpiar el select antes de agregar nuevas opciones
    select.innerHTML = "";

    razas.forEach((raza) => {
        const option = document.createElement("option");
        option.value = raza;
        option.text = raza;
        select.appendChild(option);
    });
};

const llamandoAxion = async () => {
    const url = "https://dog.ceo/api/breeds/list";

    try {
        const res = await axios.get(url);
        mostrarTodos(res.data.message);
    } catch (err) {
        console.log("Surgió un error " + err);
    }
};

const imgconAxion = async () => {
    const razaSel = document.getElementById('razasCaninas').value;
    const divImg = document.getElementById('conImg');
    const url = "https://dog.ceo/api/breed/"+razaSel+"/images/random";

    try {
        const response = await axios.get(`${url}`);
        const resultado = response.data;

        divImg.innerHTML = `<img src="${resultado.message}" alt="imgRazas">`;

    } catch (error) {
        console.log("Ocurrió un error: " + error);
    }
}

// CODIFICAMOS LOS BOTONES
document.getElementById('btnCargar').addEventListener('click', function () {
    llamandoAxion();
});

document.getElementById('btnVerImg').addEventListener('click', function () {
    imgconAxion();
});
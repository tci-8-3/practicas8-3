
const btnLimpiar = document.getElementById('btnLimpiar');


const llamandoFetch = () => {
    const url = "https://jsonplaceholder.typicode.com/todos";
    fetch(url)
        .then(respuesta => respuesta.json())  
        .then(data => mostrarDatos(data))
        .catch(reject => { 
            const lblError = document.getElementById('lblError');
            lblError.innerHTML = "Surgió un error" +reject;
        });

        const mostrarDatos = (data) => {
        const res = document.getElementById('respuesta');

        for (let item of data) {
            
            //res.innerHTML += item.userId + " " + item.id + " " + item.title + " " + item.conmplleted + "<br>"
            res.innerHTML += '<div class="container text-center"> <div class="row align-items-start">' +
                '<div class="col">'+ item.userId + '</div> <div class="col">' + item.id + '</div> <div class="col"> '
                    + item.title + '</div> <div class="col">' +item.completed+ '</div> </div> </div>'
        }
        }
};

const limpiarContenido = () => {
        const res = document.getElementById('respuesta');
        res.innerHTML = '';
        lblError.innerHTML = '';
};

document.getElementById('btnCargar').addEventListener('click',function(){
    llamandoFetch();
});

btnLimpiar.addEventListener('click', limpiarContenido);

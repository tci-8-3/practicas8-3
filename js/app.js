function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums";

    http.onreadystatechange = function() {
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);

            for (const datos of json) {
                res.innerHTML += '<tr> <td class="columna1">' + datos.userId + '</td>'
                    + '<td class="columna2">' + datos.id + '</td>'
                    + '<td class="columna3">' + datos.title + '</td> </tr>';
            }
        }
    }

    http.open('GET', url, true);
    http.send();
}

// Asignar eventos sin ejecutar las funciones
document.getElementById("btnCargar").addEventListener('click', cargarDatos);
document.getElementById("btnLimpiar").addEventListener('click', function() {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});
